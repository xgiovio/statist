var path = require('path');
var datadefinition;
var filepath;
var data = {};
var fs = require('fs'),
    bite_size = 4096,
    save_interval_or_retry,
    filereadbytes,
    tempdata = "",
    fd;

function init(file,logic,idle){
	var counterpath;

	delete require.cache[require.resolve('../../config/fromlog/datadefinitions/' + logic)];
	datadefinition = require('../../config/fromlog/datadefinitions/' + logic);

	save_interval_or_retry = idle;

	filepath = path.resolve(__dirname, '../../config/fromlog/logs/' + file);
	counterpath = path.resolve(__dirname, '../../program-data/fromlog/counters/' + file);

	fs.readFile(counterpath,function(err, content){
		if (err){
			filereadbytes = 0;
		}else{
			filereadbytes = parseInt(content);
			if (isNaN(filereadbytes)){
				filereadbytes = 0;
			}
		}
		var oldfilereadbytes = filereadbytes;
		setInterval(function(){
			if (filereadbytes != oldfilereadbytes){
				var filesplit = file.split(/\//);
				var filedirs="";
				for (var i = 0; i < filesplit.length - 1; i++) {
					filedirs+=filesplit[i];
					if (i != filesplit.length - 2){
						filedirs+="/";
					}
				}
				fs.mkdir(path.resolve(__dirname, '../../program-data/fromlog/counters',filedirs), { recursive: true }, (err) => {
				  fs.writeFile(counterpath, filereadbytes.toString(),function(err){});
				});
				oldfilereadbytes = filereadbytes;
			}
		}, save_interval_or_retry);
		openandreadsome();
	});
}


function openandreadsome(){
	fs.open(filepath, 'r',function(err, file){
		if (err){
			return setTimeout(openandreadsome, save_interval_or_retry);
		}
		fd = file;
		fs.fstat(file, function(err, stats){
			if (err || stats.size == filereadbytes){
				return fs.close(file,function(err){
					setTimeout(openandreadsome, save_interval_or_retry);
				});
			}
			if (stats.size < filereadbytes){
				filereadbytes =0;
			}
			readsome(stats);
		});
	});

}

function readsome(stats) {

	fs.read(fd, Buffer.alloc(bite_size), 0, bite_size, filereadbytes, function(err, actualbytesread, buffer){
		if (err){
			return fs.close(fd,function(err){
				setTimeout(openandreadsome, save_interval_or_retry);
			});
	
		}
		if(actualbytesread == 0) {
			return fs.close(fd,function(err){
				setTimeout(openandreadsome, save_interval_or_retry);
			});
		}
		processsome(actualbytesread,buffer);
		readsome(stats);
	});


}

function processsome(actualbytesread, buff) {
    var datastring = buff.toString('utf-8', 0, actualbytesread);
	tempdata+=datastring;
 	var tempdatasplit = tempdata.split(/\r\n|\r|\n/);
	for (var i = 0; i < tempdatasplit.length - 1; i++) {
	    data= datadefinition.extractdatafromline(data,tempdatasplit[i]);
	}
	tempdata = tempdatasplit[tempdatasplit.length - 1];
	filereadbytes+= actualbytesread;
}


module.exports = {
  init: function (file,logic,idle) {
    init(file,logic,idle);
    data= datadefinition.init(data);
  },
  data: function () {
    return data;
  }
};




