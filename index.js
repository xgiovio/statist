var http = require('http');
var path = require('path');
var configs = require("./config/config.json");

configs.forEach(function(config){
	var modifier= config.modifier;
	if (modifier=="fromlog"){
		var file = config.file;
		var logic = config.logic;
		var port = config.port;
		var idle = config.idle;
		if (isNaN(idle)){
			idle = 10000;
		}
		try{
			fromlog(file,logic,port,idle);
			console.log("Launched configuration:" + modifier + ";" + file);
		} catch (error) {
			console.log("Error launching configuration:" + modifier + ";" + file+ ";Error:" + error);
		}
	}
});


function fromlog(file,logic,port,idle){
	delete require.cache[require.resolve("./plugins/fromlog/index.js")];
	var modifierexec = require("./plugins/fromlog/index.js");
	modifierexec.init(file,logic,idle);
	createserver(modifierexec,port);
}

function createserver (modifierexec,port){

	http.createServer(function(req,res){
		res.writeHead(200,{"Content-Type": "text/plain"});
		var data = modifierexec.data();
		var body = "";
		var globallabelstring = "";
		if (data.__labels){
			for (var label in data.__labels) {
				if (data.__labels.hasOwnProperty(label)){
					globallabelstring+=label + "=\"" + data.__labels[label] + "\",";
				}
			}
		}
		for (var prop in data) {
			if (prop != "__labels" && data.hasOwnProperty(prop)) {
				var labelstring = "";
				if (data[prop].labels){
					for (var label in data[prop].labels) {
						if (data[prop].labels.hasOwnProperty(label)){
							labelstring+=label + "=\"" + data[prop].labels[label] + "\",";
						}
					}
				}
				body+="# HELP " + prop + " " + data[prop].help + "\n";
				if (data[prop].type == "counter"){
					body+="# TYPE " + prop + " counter" + "\n";
					body+=prop + "{" + globallabelstring + labelstring + "}" + " " + data[prop].value.toString() + "\n";
				}else if (data[prop].type == "gauge"){
					body+="# TYPE " + prop + " gauge" + "\n";
					body+=prop + "{" + globallabelstring + labelstring + "}" + " " + data[prop].value.toString() + "\n";
				}else if (data[prop].type == "histogram"){
					body+="# TYPE " + prop + " histogram" + "\n";
					for (var le in data[prop].bucket) {
						if (data[prop].bucket.hasOwnProperty(le)){
							body+=prop + "_bucket{le=\""+ le + "\"," + globallabelstring + labelstring + "} " + data[prop].bucket[le].toString() + "\n";
						}
					}
					body+=prop + "_count{" + globallabelstring + labelstring + "} " + data[prop].count.toString()+ "\n";
					body+=prop + "_sum{" + globallabelstring + labelstring + "} " + data[prop].sum.toString()+ "\n";
				}else if (data[prop].type == "summary"){
					body+="# TYPE " + prop + " summary" + "\n";
					for (var q in data[prop].quantile) {
						if (data[prop].quantile.hasOwnProperty(q)){
							body+=prop + "{quantile=\""+ q + "\"," + globallabelstring + labelstring + "} " + data[prop].quantile[q].toString() + "\n";
						}
					}
					body+=prop + "_count{" + globallabelstring + labelstring + "} " + data[prop].count.toString()+ "\n";
					body+=prop + "_sum{" + globallabelstring + labelstring + "} " + data[prop].sum.toString()+ "\n";
				}
			}
		}
		res.end(body);
	}).listen(port);
}



