module.exports = {
	init: function(data){
		/* counter sample
		data.name = {}
		data.name.help = "help";
		data.name.type = "counter";
		data.name.value = 0;
		*/

		/* gauge sample
		data.name = {}
		data.name.help = "help";
		data.name.type = "gauge";
		data.name.value= 0;
		*/

		/* histogram sample
		data.name = {}
		data.name.help = "help";
		data.name.type = "histogram";
		data.name.bucket = {};
		data.name.sum = 0;
	 	data.name.count = 0;
		*/

		/* summary sample
		
		data.name = {}
		data.name.help = "help";
		data.name.type = "summary";
		data.name.quantile = {};
		data.name.sum = 0;
	 	data.name.count = 0;
		*/


		/* global label sample
		data.__labels = {}
		data.__labels.test1="aa";
		data.__labels.test2="bb";
		*/

		/* per metric label sample
		data.name.labels = {}
		data.name.labels.test3="aa";
		data.name.labels.test4="bb";
		*/



		return data;
	},
	extractdatafromline: function(data,line){

		/* counter sample
		data.name.value++;
		*/

		/* gauge sample
		data.name.value= 10;
		*/

		/* histogram sample
		data.name.bucket["0.1"] = 3;
		data.name.bucket["0.5"] = 5;  
		data.name.bucket["+Inf"] = 5; 
		data.name.sum = 1.3;
	 	data.name.count = 5;
		*/

		/* summary sample
		data.name.quantile["0.95"] = 3;
		data.name.sum = 1.3;
	 	data.name.count = 5;
		*/


		return data;

	}
};
